<?php
/**
 * CandleStickDenormalizer.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\BinanceMinus\Model\Normalizer;

use AzureSpring\BinanceMinus\Model\CandleStick;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * CandleStickDenormalizer.
 */
class CandleStickDenormalizer implements DenormalizerInterface
{
    /**
     * {@inheritDoc}
     *
     * @throws \Exception
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
        list($openAt, $openPrice, $hiPrice, $loPrice, $closePrice, $qty, $closeAt, $vol, $nr,
            $takerQty, $takerVol, $ignore) = $data;

        return (new CandleStick())
            ->setOpenAt((new \DateTimeImmutable())->setTimestamp($openAt / 1e3))
            ->setCloseAt((new \DateTimeImmutable())->setTimestamp($closeAt / 1e3))
            ->setOpenPrice($openPrice)
            ->setClosePrice($closePrice)
            ->setHiPrice($hiPrice)
            ->setLoPrice($loPrice)
            ->setVolume($vol)
            ->setQuantity($qty)
            ->setCount($nr)
            ;
    }

    /**
     * {@inheritDoc}
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === CandleStick::class;
    }
}
