<?php
/**
 * CandleStick.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\BinanceMinus\Model;

/**
 * CandleStick.
 */
class CandleStick
{
    const INTERVAL_1_MINUTE   =  '1m';
    const INTERVAL_3_MINUTES  =  '3m';
    const INTERVAL_5_MINUTES  =  '5m';
    const INTERVAL_15_MINUTES = '15m';
    const INTERVAL_30_MINUTES = '30m';

    const INTERVAL_1_HOUR     =  '1h';
    const INTERVAL_2_HOURS    =  '2h';
    const INTERVAL_4_HOURS    =  '4h';
    const INTERVAL_6_HOURS    =  '6h';
    const INTERVAL_8_HOURS    =  '8h';
    const INTERVAL_12_HOURS   = '12h';

    const INTERVAL_1_DAY      =  '1d';
    const INTERVAL_3_DAYS     =  '3d';

    const INTERVAL_1_WEEK     =  '1w';
    const INTERVAL_1_MONTH    =  '1M';

    /**
     * @var \DateTimeImmutable
     */
    private $openAt;

    /**
     * @var \DateTimeImmutable
     */
    private $closeAt;

    /**
     * @var string
     */
    private $openPrice;

    /**
     * @var string
     */
    private $closePrice;

    /**
     * @var string
     */
    private $hiPrice;

    /**
     * @var string
     */
    private $loPrice;

    /**
     * @var string
     */
    private $volume;

    /**
     * @var string
     */
    private $quantity;

    /**
     * @var string
     */
    private $count;

    /**
     * @return \DateTimeImmutable
     */
    public function getOpenAt()
    {
        return $this->openAt;
    }

    /**
     * @param \DateTimeImmutable $openAt
     *
     * @return $this
     */
    public function setOpenAt($openAt)
    {
        $this->openAt = $openAt;

        return $this;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCloseAt()
    {
        return $this->closeAt;
    }

    /**
     * @param \DateTimeImmutable $closeAt
     *
     * @return $this
     */
    public function setCloseAt($closeAt)
    {
        $this->closeAt = $closeAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getOpenPrice()
    {
        return $this->openPrice;
    }

    /**
     * @param string $openPrice
     *
     * @return $this
     */
    public function setOpenPrice($openPrice)
    {
        $this->openPrice = $openPrice;

        return $this;
    }

    /**
     * @return string
     */
    public function getClosePrice()
    {
        return $this->closePrice;
    }

    /**
     * @param string $closePrice
     *
     * @return $this
     */
    public function setClosePrice($closePrice)
    {
        $this->closePrice = $closePrice;

        return $this;
    }

    /**
     * @return string
     */
    public function getHiPrice()
    {
        return $this->hiPrice;
    }

    /**
     * @param string $hiPrice
     *
     * @return $this
     */
    public function setHiPrice($hiPrice)
    {
        $this->hiPrice = $hiPrice;

        return $this;
    }

    /**
     * @return string
     */
    public function getLoPrice()
    {
        return $this->loPrice;
    }

    /**
     * @param string $loPrice
     *
     * @return $this
     */
    public function setLoPrice($loPrice)
    {
        $this->loPrice = $loPrice;

        return $this;
    }

    /**
     * @return string
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * @param string $volume
     *
     * @return $this
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param string $quantity
     *
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return string
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param string $count
     *
     * @return $this
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }
}
