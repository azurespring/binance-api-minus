<?php
/**
 * Client.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\BinanceMinus;

use AzureSpring\BinanceMinus\Model\CandleStick;
use AzureSpring\BinanceMinus\Model\Normalizer\CandleStickDenormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Binance minus client, the.
 */
class Client implements ClientInterface
{
    /**
     * @var \GuzzleHttp\ClientInterface
     */
    private $guzzle;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * Constructor.
     *
     * @param \GuzzleHttp\ClientInterface $guzzle
     */
    public function __construct(\GuzzleHttp\ClientInterface $guzzle = null)
    {
        $this->guzzle = $guzzle;
        $this->serializer = new Serializer(
            [new CandleStickDenormalizer(), new ArrayDenormalizer()],
            [new JsonEncoder()]
        );
    }

    /**
     * {@inheritdoc}
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getKlines(string $symbol, string $interval, ?\DateTimeInterface $start = null, ?\DateTimeInterface $end = null, ?int $limit = null): array
    {
        return $this->serializer->deserialize(
            $this
            ->guzzle
            ->request('GET', '/api/v1/klines', [
                'query' => [
                    'symbol'    => $symbol,
                    'interval'  => $interval,
                    'startTime' => $start ? $start->getTimestamp() * 1e3 : null,
                    'endTime'   => $end ? $end->getTimestamp() * 1e3 : null,
                    'limit'     => $limit,
                ],
            ])
            ->getBody()
            ->getContents(),
            CandleStick::class.'[]',
            'json'
        );
    }
}
