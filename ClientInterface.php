<?php
/**
 * ClientInterface.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\BinanceMinus;

use AzureSpring\BinanceMinus\Model\CandleStick;

/**
 * ClientInterface.
 */
interface ClientInterface
{
    const BASE_URI = 'https://api.binance.com';

    /**
     * Get klines.
     *
     * @param string                  $symbol
     * @param string                  $interval CandleStick::INTERVAL_*
     * @param \DateTimeInterface|null $start
     * @param \DateTimeInterface|null $end
     * @param int|null                $limit
     *
     * @return CandleStick[]
     */
    public function getKlines(string $symbol, string $interval, ?\DateTimeInterface $start = null, ?\DateTimeInterface $end = null, ?int $limit = null): array;
}
