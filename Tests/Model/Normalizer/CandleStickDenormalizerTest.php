<?php
/**
 * CandleStickDenormalizerTest.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\BinanceMinus\Tests\Model\Normalizer;

use AzureSpring\BinanceMinus\Model\CandleStick;
use AzureSpring\BinanceMinus\Model\Normalizer\CandleStickDenormalizer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * CandleStickDenormalizerTest.
 */
class CandleStickDenormalizerTest extends TestCase
{
    /**
     * Denormalize.
     *
     * @covers \AzureSpring\BinanceMinus\Model\Normalizer\CandleStickDenormalizer
     * @covers \AzureSpring\BinanceMinus\Model\CandleStick
     */
    public function testDenormalize()
    {
        $serializer = new Serializer(
            [new CandleStickDenormalizer(), new GetSetMethodNormalizer(), new ArrayDenormalizer()],
            [new JsonEncoder()]
        );

        /**
         * @var CandleStick $candleStick
         */
        $candleStick = $serializer->deserialize(
            '[1534809600000,"6251.00000000","6490.00000000","6235.08000000","6412.91000000","34305.48338300",1534895999999,"218655424.16002883",137884,"19309.09984300","123074004.71473382","0"]',
            CandleStick::class,
            'json'
        );

        $this->assertInstanceOf(CandleStick::class, $candleStick);
        $this->assertEquals(1534809600, $candleStick->getOpenAt()->getTimestamp());
        $this->assertEquals(1534895999, $candleStick->getCloseAt()->getTimestamp());
        $this->assertEquals('6251.00000000', $candleStick->getOpenPrice());
        $this->assertEquals('6412.91000000', $candleStick->getClosePrice());
        $this->assertEquals('6490.00000000', $candleStick->getHiPrice());
        $this->assertEquals('6235.08000000', $candleStick->getLoPrice());
        $this->assertEquals('218655424.16002883', $candleStick->getVolume());
        $this->assertEquals('34305.48338300', $candleStick->getQuantity());
        $this->assertEquals('137884', $candleStick->getCount());
    }
}
