<?php
/**
 * ClientTest.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\BinanceMinus\Tests;

use AzureSpring\BinanceMinus\Client;
use AzureSpring\BinanceMinus\Model\CandleStick;
use GuzzleHttp\Psr7\BufferStream;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

/**
 * ClientTest.
 */
class ClientTest extends TestCase
{
    /**
     * @covers \AzureSpring\BinanceMinus\Client
     * @covers \AzureSpring\BinanceMinus\Model\CandleStick
     * @covers \AzureSpring\BinanceMinus\Model\Normalizer\CandleStickDenormalizer
     *
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testGetKlines()
    {
        $s = new BufferStream();
        $s->write(
            <<<__JSON_RESPONSE__
[[1534723200000,"6477.53000000","6530.00000000","6220.00000000","6254.84000000","49435.55526000",1534809599999,"317617945.74926080",237055,"26468.01709100","170185693.09984116","0"]]
__JSON_RESPONSE__
        );

        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')
            ->willReturn($s)
        ;
        $guzzle = $this->createMock(\GuzzleHttp\ClientInterface::class);
        $guzzle->expects($this->once())
            ->method('request')
            ->with($this->equalTo('GET'), $this->equalTo('/api/v1/klines'), $this->equalTo([
                'query' => [
                    'symbol'    => 'BTCUSDT',
                    'interval'  => '1d',
                    'startTime' => null,
                    'endTime'   => 1534809600000,
                    'limit'     => 1,
                ],
            ]))
            ->willReturn($response)
        ;

        $klines = (new Client($guzzle))->getKlines(
            'BTCUSDT',
            CandleStick::INTERVAL_1_DAY,
            null,
            new \DateTimeImmutable('2018-08-21Z'),
            1
        );

        $this->assertCount(1, $klines);
    }
}
